const express = require("express");
const app = express();

//  dummy todolist
let todosList = ['task1', 'task2', 'task3'];

//  parse form data
app.use(express.urlencoded({ extended: false }));
//  parse json
app.use(express.json());

//  Home
app.get("/", (req, res) => {
  res
    .status(200)
    .send(
      '<a href="/display"> display </a> \n <a href="/add"> add </a> \n <a href="/update"> update </a> \n <a href="/delete"> delete </a>'
    );
});

//  display/get
app.get("/display", (req, res) => {
  res.status(200).send(todosList);
});

//  add/post
/*
to add a task to the list 
post > body > raw > json
{
    "task": "The task you want to update "
}
*/
app.post("/add", (req, res) => {
  todosList.length > 0 ? id=todosList.length : id=0
  const newTask = {
    id: id,
    task: req.body.task,
    deadline: 'today',
    time: '12.30h'
  };
  todosList.push(newTask);
  res.status(200).json({ success: true, data: todosList });
});

//  update/put
/*
to update a task on the list 
put > body > raw > json
{
    "task": "Task you want to edit ",
    "newtask": "New task"
}
*/
app.put("/update", (req, res) => {
  const { task } = req.body;
  const { newtask } = req.body;

  const taskUpdate = todosList.find(
    (taskUpdate) => taskUpdate.task === task
  );

  if (!taskUpdate) {
    return res
      .status(404)
      .json({ success: false, msg: `no task with name ${task}` });
  }
  todosList = todosList.map((taskUpdate) => {
    if (taskUpdate.task === task) {
      taskUpdate.task = newtask;
    }
    return taskUpdate;
  });
  res.status(200).json({ success: true, data: todosList });
});

//  delete
/*
to delete a task on the list 
delete > body > raw > json
{
    "task": "The task you want to delete "
}
*/
app.delete("/delete", (req, res) => {
  const taskDelete = todosList.find(
    (taskDelete) => taskDelete.task === req.body.task
  );
  if (!taskDelete) {
    return res
      .status(404)
      .json({ success: false, msg: `no task with name ${req.body.task}` });
  }
  todosList = todosList.filter(
    (taskDelete) => taskDelete.task !== req.body.task
  );
  return res.status(200).json({ success: true, data: todosList });
});

//  listen
app.listen(3000, () => {
  console.log("Your are now listening on port 3000...");
});
